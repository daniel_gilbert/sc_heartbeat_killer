
/*
  Solar Controller Heartbeat Killer. Created for the TKLN Solar Project Jan 2020.
  
  Daniel Gilbert
*/
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fstream>
#include <errno.h>
#include <sys/fcntl.h>
#include <sstream>
#include <sys/ioctl.h>
#include <math.h>


using namespace std;

bool display_messages, solarcontroller_connected;
int old_heartbeat, new_heartbeat;
int solarcontroller_socket, init_socket(), clear_socket_input(int socket_fd);

int clear_socket_input(int socket) {
    int bytes;
    int reads = 0;
    ioctl(socket, FIONREAD, &bytes);

    while (bytes > 0) {
        if (read(socket, &bytes, 1) >= 0)
            ioctl(socket, FIONREAD, &bytes);
        else
            bytes = -1; //read error
        if (++reads > 800)
            bytes = -2; //there's too much on the socket
    }
    return bytes;
}

int modbus_poll_and_response(int socketfd, unsigned char function_code, unsigned char transaction_id, unsigned char unit_id, unsigned short starting_reg_remote, unsigned char num_of_regs) {

  unsigned char mod_poll[12];
  mod_poll[0] = 0x00; // Transaction ID
  mod_poll[1] = transaction_id;
  mod_poll[2] = 0x00; // Always zero for MODBUS TCP/IP
  mod_poll[3] = 0x00;
  mod_poll[4] = 0x00; // Number of bytes remaining
  mod_poll[5] = 0x06;
  mod_poll[6] = unit_id; // Unit ID
  mod_poll[7] = 0x03; // Modbus tcp function code
  mod_poll[8] = 0x00FF & (starting_reg_remote >> 8); //start address
  mod_poll[9] = 0x00FF & (starting_reg_remote);
  mod_poll[10] = 0x00; // Quantity of registers
  mod_poll[11] = num_of_regs;

  
  /* Clear input buffer ready for reply*/
  int n = clear_socket_input(socketfd);

  if (n < 0) {
    if (display_messages)
      cout << "ERROR: Problem reading from socket " << n << endl;

    close(socketfd);
    return -1;
  }

  write(socketfd, mod_poll, 12);

  /* Poll until bytes are available*/
  int bytes = 0;
  int r = 0;
  int expected_response_length = 1;
  int expected_data_length = 1;

  //Work out expected length of reply based on function code
    expected_response_length = (((int)num_of_regs) * 2 + 9);
    expected_data_length = (((int)num_of_regs) * 2);
	

  while ((bytes < expected_response_length) && (bytes >= 0)) {
    /* How much data is in the buffer*/
    usleep(20000);
    ioctl(socketfd, FIONREAD, &bytes);
    r++;
    if (r >= 25) {
      if (display_messages)
        cout << "ERROR: Bytes in input buffer never correct" << endl;
      bytes = -1;
    }
  }

  unsigned char mod_response[4096];
  if (bytes >= 0) {
    n = read(socketfd, mod_response, expected_response_length);
  }
  else {
    n = -1;
  }

  if (n < 0) {
    if (display_messages)
      cout << "ERROR: Problem reading from socket " << n << endl;
    close(socketfd);
    return -1;
  }



  if (display_messages) {
    cout << "modpol response: " << endl;
    cout << (int)mod_response[0] << endl;
    cout << (int)mod_response[1] << endl;
    cout << (int)mod_response[2] << endl;
    cout << (int)mod_response[3] << endl;
	cout << (int)mod_response[4] << endl;
    cout << (int)mod_response[5] << endl;
	cout << (int)mod_response[6] << endl;
    cout << (int)mod_response[7] << endl;
	cout << (int)mod_response[8] << endl;
    cout << (int)mod_response[9] << endl;
	cout << (int)mod_response[10] << endl;
  }


  //Checks if the transaction ID and listed number of bytes are matching & store response
  if ((function_code != 0x01 && (transaction_id == mod_response[1]) && ((unsigned char)expected_data_length == (unsigned char)mod_response[8]))) {
	 
	new_heartbeat = ((((int)mod_response[9]) << 8) | ((int)mod_response[10])); //new_heartbeat = mod_response
		  
    return 1;
  }


  if (display_messages) {
    cout << "ERROR: TransactionID didn't match or different length data" << endl;
    cout << "expected transaction_id =" << transaction_id << endl;
    cout << "mod_response transaction_id =" << mod_response[1] << endl;
    cout << "expected data length =" << (unsigned char)expected_data_length << endl;
    cout << "mod_response data length =" << (unsigned char)mod_response[8] << endl;
  }

  close(socketfd);
  return -1;
}


/* Initialise Socket */
int init_socket() {

    sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(502);
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

    int new_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (new_socket < 0) {
        cout << "ERROR creating socket at:" << "127.0.0.1" << endl;
    }
    else {
        int connected = false;
        fcntl(new_socket, F_SETFL, O_NONBLOCK); //Set to Non Blocking IO so that connect does not hang
        for (int i = 0; i < 5 && !connected; i++) { //System will try every one second for 5 seconds. After that break
            if (connect(new_socket, (sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
                cout << "Attempting to connect to " << "127.0.0.1" << endl;
                usleep(1000000); //sleep 1sec
            }
            else {
                cout << "Connected to " << "127.0.0.1" << endl;
                connected = true;
            }
        }
        fcntl(new_socket, F_SETFL, O_ASYNC); //Bring back to blocking IO

        if (!connected) {
            cout << "ERROR connecting to socket at " << "127.0.0.1" << endl;
        return -1;
		} else {
            struct timeval timeout;
            timeout.tv_sec = 1; //set read timeout to 1 second
            timeout.tv_usec = 0;
            setsockopt(new_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
        }
    }
    return new_socket;
}


int main(int argc, char * argv[]) {

  unsigned int increment = 10;

  nice(-17);

	display_messages = false;
	old_heartbeat = 4000;
	new_heartbeat = 5000;
	int counter = 0;

	//init solarcontroller socket first
      solarcontroller_socket = init_socket();
      if (solarcontroller_socket > 0 ) {
		solarcontroller_connected = true;
	  }
	  else {
		solarcontroller_socket = false;
	  }
	  cout << "Solarcontroller connected flag : " << solarcontroller_connected << endl;




  /* Start of while loop */
  while (true) {

	
    if (!solarcontroller_connected) {
      close(solarcontroller_socket);
      solarcontroller_socket = init_socket();
      if (solarcontroller_socket > 0 ) {
		solarcontroller_connected = true;
	  }
	  else {
		solarcontroller_socket = false;
	  }  
	  cout << "Solarcontroller connected flag : " << solarcontroller_connected << endl;
    }

	old_heartbeat = new_heartbeat;
	
	if (modbus_poll_and_response(solarcontroller_socket, 0x03, increment++, 0x01, 277-1, 2) > 0) {
		if (display_messages) {
			cout << "Modpol OK." << endl;
		}		
		} else {
			solarcontroller_connected = false;
			if (display_messages) {
				cout << "Failed to modpol." << endl;
			}	
	};
    
	if (display_messages) {
		cout << "Old heartbeat: " << old_heartbeat << endl;
		cout << "New heartbeat: " << new_heartbeat << endl;
		cout << "Counter: " << counter << endl;
	}	
	
	if (old_heartbeat == new_heartbeat) {
		counter++; //Gives it 10 minutes to get a heartbeat going again before killing
		if (counter > 60) {
			cout << "Oh boy! Here I go, killing again." << endl;
			int system(const char*command);
			string str = "killall solarcontroller_run";
			const char * command = str.c_str();
			system(command); //killall solar_controller_run;
			counter = 0;
		}
	}
		
		
	if (increment > 50) {
			increment = 1;
	}
	
    usleep(10000000); //10s sleep
  }
}